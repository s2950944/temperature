package nl.utwente.di.bookQuote;

import java.io.*;

import com.sun.java.accessibility.util.Translator;
import jakarta.servlet.*;
import jakarta.servlet.http.*;

/** Example of a Servlet that gets an ISBN number and returns the book price
 */

public class TemperatureTranslator extends HttpServlet {
 	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private TempTranslator translator;
	
    public void init() throws ServletException {
    	translator = new TempTranslator();
    }	
	
  public void doGet(HttpServletRequest request,
                    HttpServletResponse response)
      throws ServletException, IOException {

    response.setContentType("text/html");
    PrintWriter out = response.getWriter();
    String title = "Temperature Converted";
    
    // Done with string concatenation only for the demo
    // Not expected to be done like this in the project
    out.println("<!DOCTYPE HTML>\n" +
                "<HTML>\n" +
                "<HEAD><TITLE>" + title + "</TITLE>" +
                "<LINK REL=STYLESHEET HREF=\"styles.css\">" +
                		"</HEAD>\n" +
                "<BODY BGCOLOR=\"#FDF5E6\">\n" +
                "<H1>" + title + "</H1>\n" +              
                "  <P>Temperature (in Celsius): " +
                   request.getParameter("tmp") + "\n" +
                "  <P>Converting to temperature (in Faraheit): " +
                   translator.convert(request.getParameter("tmp")) +
                "</BODY></HTML>");
  }
}
