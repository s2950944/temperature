package nl.utwente.di.bookQuote;

public class TempTranslator {

    public TempTranslator() {

    }
    public double convert(String temp) {
        double result = Double.parseDouble(temp);
        result = (result*9/5) + 32;

        return result;
    }

    public void main(String[] args) {

    }

}
