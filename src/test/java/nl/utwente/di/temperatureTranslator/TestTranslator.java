package nl.utwente.di.temperatureTranslator;

import nl.utwente.di.bookQuote.TempTranslator;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * Test the Translator
 */
public class TestTranslator {
    @Test
    public void testBook1() throws Exception {
        TempTranslator translator = new TempTranslator();
        double temperature = translator.convert("37");
        Assertions.assertEquals(98.6, temperature, 0.0, "Converted Temperature");
    }
}